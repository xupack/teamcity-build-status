function trim(text) {
    if (text.length > 50) {
        return text.substring(0, 50) + '…';
    }

    return text;
}

function openTab(url) {
    return function() {
        chrome.tabs.create({
            url: url
        });
    }
}

function toLink() {
    var ar = Array.prototype.slice.call(arguments);
    if (ar.length == 1) {
        return "" + ar[0];
    } else if (ar.length == 2) {
        return ar[0] + ar[1];
    }

    var base = ar[0];
    var format = ar[1];
    var args = ar.slice(2);
    return base + vsprintf(format, args)
}

function saveJson(key, obj) {
    localStorage[key] = JSON.stringify(obj)
}

function loadJson(key, obj) {
    var raw = localStorage[key];
    if (raw) {
        return JSON.parse(raw)
    }
    return obj;
}

function openLink() {
    var args = [];
    for (var i = 0; i < arguments.length; i++) {
        args.push(arguments[i])
    }

    return openTab(toLink.apply(this, arguments))
}

String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

String.prototype.startsWith = function(prefix) {
    return this.slice(0, prefix.length) == prefix;
};
